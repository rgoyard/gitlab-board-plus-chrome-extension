// this is the code which will be injected into a given page...
function findAncestor(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls)) ;
    return el;
}

function getCarFromButton(button) {
    return findAncestor(button, 'card');
}

/**
 *
 * @param buttons
 * @param label
 */
function changeColorOfCardWithLabel(buttons, label) {
    if (buttons) {
        buttons.map((b) => {
            const card = getCarFromButton(b);
            if (Array.prototype.slice.call(card.classList).indexOf('gitlab-boardplus-extension-colored') < 0) {
                card.style.backgroundColor = label.color;
                card.classList.add('gitlab-boardplus-extension-colored');
            }
        });
    }
}


/**
 * This function is used to check if a colored label has been removed
 * @param labels
 */
function removeColorOnUnLabeledCards(labels) {
    const colored = Array.prototype.slice.call(document.querySelectorAll('.gitlab-boardplus-extension-colored'));
    colored.forEach((card) => {
        let color = cardShouldBeColored(card, labels);
        if (color) {
            card.style.backgroundColor = color;
        } else {
            card.style.backgroundColor = null;
            card.classList.remove('gitlab-boardplus-extension-colored');
        }
    })
}

/**
 * Check id card contains
 * @param card
 * @param labels
 * @returns {boolean}
 */
function cardShouldBeColored(card, labels) {
    const buttons = Array.prototype.slice.call(card.querySelectorAll('.card-footer button'));
    let color = null;
    labels.forEach((label) => {
        const found = buttons.find((b) => b.innerText.toUpperCase() === label.label.toUpperCase());
        if (found) {
            color = label.color;
        }
    });
    return color;
}

function checkWipIt(board) {
    const headerText = board.querySelector('header h3 .board-title-text');
    const headerCount = board.querySelector('header h3 .issue-count-badge-count');
    const text = headerText.innerText;
    const count = headerCount.innerText;

    // check if have a wip
    const reg = /(.*)\((\d)\)/ig;
    const res = reg.exec(text);
    if (res && res.length >= 3) {
        const wip = parseInt(res[2], 10);
        const header = board.querySelector('header');
        if (wip < parseInt(count, 10)) {
            header.style.backgroundColor = '#F00';
        } else {
            header.style.backgroundColor = null;
        }
    }
}

function wipIt() {
    const glBoards = document.querySelectorAll('.board');
    Array.prototype.slice.call(glBoards).forEach((board) => {
        checkWipIt(board);
    })
}

function getListButtons(labels, callback) {
    let list = Array.prototype.slice.call(document.querySelectorAll('li.card .card-footer button'));
    const buttons = {};
    labels.forEach((label) => {
        if (!buttons.hasOwnProperty(label.label)) {
            buttons[label.label] = {label};
        }
        buttons[label.label].elements = list.filter((b) => b.innerText.toUpperCase() === label.label.toUpperCase());
    });
    callback(buttons);
}

function load(labels) {
    getListButtons(labels, (buttons) => {
        Object.keys(buttons).forEach((b) => {
            changeColorOfCardWithLabel(buttons[b].elements, buttons[b].label);
        });
    });
    wipIt();
}

(function () {
    chrome.storage.sync.get({
        projects: [],
    }, (items) => {
        const currentUrl = window.location.href;
        const currentProject = items.projects.find((p) => p.boardUrl.startsWith(currentUrl));
        if (currentProject) {
            console.log(currentProject);
            const labels = currentProject.tags;
            load(labels);
            window.addEventListener('click', () => {
                removeColorOnUnLabeledCards(labels);
                load(labels);
            });

        } else {
            console.log('Project not found');
            console.log(items);
        }
    });

})();
