// this is the background code...

// listen for our browerAction to be clicked
chrome.browserAction.onClicked.addListener(function(activeTab)
{
    chrome.tabs.create({ url: 'src/options/option.html' });
});

function changeCardColor() {
    chrome.tabs.executeScript({
        file: 'src/inject/inject.js'
    });
}

chrome.tabs.onUpdated.addListener((tabId) => {
    load(tabId)
});
chrome.tabs.onActivated.addListener((activeInfo) => {
    load(activeInfo.tabId)
});

function load(tabId) {
    // how to fetch tab url using tabId.tabid
    chrome.storage.sync.get({
        projects: [],
    }, (items) => {
        const urls = items.projects.map((b) => b.boardUrl);
        chrome.tabs.get(tabId, (tab) => {
            if (urls.indexOf(tab.url) >= 0) {
                changeCardColor();
            }
        });
    });
}
