(function ($) {
    const serverURL = 'https://gitlab-board-plus-extension.herokuapp.com';

    function restore_options() {
        chrome.storage.sync.get({
            projects: [],
        }, function (items) {
            if (Array.isArray(items.projects)) {
                init(items.projects);
                items.projects.forEach((p) => {
                    refreshProjectData(p.id, items.projects);
                })
            } else {
                chrome.storage.sync.set({
                    projects: [],
                }, function () {
                    init({projects: []});
                });
            }

        });
    }

    /**
     * Create the tags in the list.
     *
     * @param tags
     * @returns {string}
     */
    function createTags(tags) {
        let buff = '';
        tags.forEach((tag) => {
            buff += '<span class="color-display" style="background-color:' + tag.color + '; ">';
            buff += tag.label;
            buff += '</span>';
        });
        return buff;
    }

    /**
     * Create a row for the list of projects.
     *
     * @param project
     * @returns {string}
     */
    function createRowForProject(project) {
        let buff = '';
        buff += `<tr data-id="${project.id}">`;
        buff += '<td>';
        buff += project.name;
        buff += '</td>';
        buff += '<td>';
        buff += createTags(project.tags);
        buff += '</td>';
        buff += '<td>';
        buff += `<a href="${project.boardUrl}" target="_blank" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Go to the ${project.name} board"><i class="material-icons">link</i></a>`;
        buff += `<button class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Reload settings of ${project.name} board"><i class="material-icons">cached</i></button>`;
        buff += `<a href="${serverURL}/project/${project.id}" target="_blank" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Go to ${project.name} board settings page"><i class="material-icons">settings</i></a>`;
        buff += `<button class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete the board ${project.name}"><i class="material-icons">delete</i></button>`;
        buff += '</td>';
        buff += '</tr>';
        return buff;
    }

    function deleteProject(id, projects) {
        const projectsWithout = projects.filter(p => p.id !== id);
        chrome.storage.sync.set({
            projects: projectsWithout,
        }, function () {
            init(projectsWithout);
        });
    }

    function refreshProjectData(id, projects) {
        const project = projects.find(p => p.id === id);
        if (project) {
            const url = project.url;
            $.getJSON(url)
                .then((projectServer) => {
                    projectServer.url = url;
                    const modifiedProjects = projects.map((p) => {
                        if (p.id === id) {
                            return projectServer
                        } else {
                            return p;
                        }
                    });
                    chrome.storage.sync.set({
                        projects: modifiedProjects,
                    }, function () {
                        listProjects(modifiedProjects);
                    });
                })
                .catch((err) => {
                    console.log(err);
                })
        } else {
            console.log('Unable to find project');
        }
    }

    function listProjects(projects) {
        let buff = '';
        projects.forEach((project) => {
            buff += createRowForProject(project);
        });

        if (buff === '') {
            buff += `<tr><td colspan="3">Please attach a board</td></tr>`;
        }
        $('#listOfBoards').html(buff);
        $('#listOfBoards .btn').tooltip();
    }

    function init(projects) {
        $('#formAdd').submit(function (ev) {
            ev.preventDefault();
            const url = document.getElementById('urlProject').value;
            const projectWithUrl = projects.find((p) => p.url === url);
            if (!projectWithUrl) {
                $.getJSON(url)
                    .then((project) => {
                        project.url = url;
                        projects.push(project);
                        chrome.storage.sync.set({
                            projects,
                        }, function () {
                            console.log('saved to chrome');
                            listProjects(projects);
                        });
                    })
                    .catch((err) => {
                        console.log(err);
                    })
            } else {
                refreshProjectData(projectWithUrl.id, projects);
            }
        });
        $('#listOfBoards').on('click', 'tr button.btn-danger', function () {
            const id = $(this).parents('tr').data('id');
            deleteProject(id, projects);
        });
        $('#listOfBoards').on('click', 'tr button.btn-info', function () {
            const id = $(this).parents('tr').data('id');
            refreshProjectData(id, projects)
        });


        $('.help-button').click(function () {
            $('#help-menu').click();
        });
        listProjects(projects);
    }

    document.addEventListener('DOMContentLoaded', restore_options);

})
(jQuery);